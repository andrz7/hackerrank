package WeekOfCode;

import java.util.Scanner;

/**
 * Created by SRIN on 6/29/2016.
 */
public class Kangaroo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x1 = in.nextInt();
        int v1 = in.nextInt();
        int x2 = in.nextInt();
        int v2 = in.nextInt();


        if (isMet(x1, v1, x2, v2)){
            System.out.println("YES");
        }else{
            System.out.println("NO");
        }

    }

    public static boolean isMet(int x1, int v1, int x2, int v2){
        while (true){
            if (x1 == x2) // if kangarooo has same dist
                return true;
            if (x1 == x2 && v1 == v2) // if kangaroo has sam dist and speed
                return true;
            if (x2 > x1 && v2 > v1) // if kangaroo has dif speed with diff dist
                return false;
            if (x1 < x2 && v1 > v2) // if kanggaroo has less speed and more speed
                return true;
            x1 += v1;
            x2 += v2;
        }
    }
}
