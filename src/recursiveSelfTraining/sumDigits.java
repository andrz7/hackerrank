package recursiveSelfTraining;

import java.util.Scanner;

/**
 * Created by SRIN on 7/19/2016.
 */
public class sumDigits {
    public sumDigits() {
        Scanner scan = new Scanner(System.in);
        System.out.println(sumDigits(scan.nextInt()));
    }

    public static void main(String[] args){
        new sumDigits();
    }

    public int sumDigits(int n) {
        if (n < 10)
            return n;

        return sumDigits(n % 10) + sumDigits(n / 10);
    }
}
