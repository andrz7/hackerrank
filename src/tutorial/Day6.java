package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/7/2016.
 */
public class Day6 {

    public Day6() {
        Scanner scan = new Scanner(System.in);
        int T = scan.nextInt();scan.nextLine();
        String evenString, oddString, normalString;

        for (int i = 1 ; i <= T ; i++){
            evenString = "";
            oddString = "";
            normalString = scan.nextLine();
            for (int j = 0 ; j < normalString.length() ; j++){
                if (j % 2 == 0 ) evenString += normalString.charAt(j);
                else if (j % 2 == 1) oddString += normalString.charAt(j);
                else evenString += normalString.charAt(j);
            }
            System.out.println(evenString + " " + oddString);
        }

    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        new Day6();
    }
}
