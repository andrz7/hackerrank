package tutorial;

/**
 * Created by SRIN on 6/22/2016.
 */
public class Day21 {


    static <E> void printArray(E[] array){
        for (E element : array){
            System.out.println(element);
        }
    }

    public static void main(String args[]){
        Integer[] intArray = { 1, 2, 3 };
        String[] stringArray = { "Hello", "World" };

        printArray( intArray  );
        printArray( stringArray );

        if(Day21.class.getDeclaredMethods().length > 2){
            System.out.println("You should only have 1 method named printArray.");
        }
    }
}
