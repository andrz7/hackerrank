package tutorial;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by SRIN on 6/20/2016.
 */
public class Day18 {
    static Stack<Character> stackOfChar;
    static Queue<Character> queueOfChar;

    public Day18() {
        queueOfChar = new LinkedList<>();
        stackOfChar = new Stack<>();
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        scan.close();

        // Convert input String to an array of characters:
        char[] s = input.toCharArray();
        // Create a Solution object:
        Day18 p = new Day18();

        // Enqueue/Push all chars to their respective data structures:
        for (char c : s) {
            p.pushCharacter(c);
            p.enqueueCharacter(c);
        }

        // Pop/Dequeue the chars at the head of both data structures and compare them:
        boolean isPalindrome = true;
        for (int i = 0; i < s.length/2; i++) {
            if (p.popCharacter() != p.dequeueCharacter()) {
                isPalindrome = false;
                break;
            }
        }

        //Finally, print whether string s is palindrome or not.
        System.out.println( "The word, " + input + ", is "
                + ( (!isPalindrome) ? "not a palindrome." : "a palindrome." ) );
    }

    private char dequeueCharacter() {
        return queueOfChar.poll();
    }

    private char popCharacter() {
        return stackOfChar.pop();
    }


    private void enqueueCharacter(char c) {
        queueOfChar.add(c);
    }

    private void pushCharacter(char c) {
        stackOfChar.add(c);
    }
}
