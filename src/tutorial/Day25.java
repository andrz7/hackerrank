package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/28/2016.
 */
public class Day25 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int T = scan.nextInt();

        while (T-- > 0){
            int number = scan.nextInt();
            System.out.println(checkedPrime(number));
        }
    }

    public static String checkedPrime(int number){
        if (number == 1)
            return "Not prime";
        if (number == 2)
            return "Prime";
        if (number % 2 == 0)
            return "Not prime";
       for (int i = 3 ; i*i < number; i+=2){
            if (number % i == 0)
                return "Not prime";
       }
        return "Prime";
    }
}
