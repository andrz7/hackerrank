package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/7/2016.
 */
public class Loop {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        for (int i = 1 ; i <= 10 ; i++){
            int result = N * i;
            System.out.println(String.format("%d x %d = %d", N, i, result));
        }
    }
}
