package tutorial;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by SRIN on 6/14/2016.
 */
public class Day14 {

    static class Difference {
        private int[] elements;
        private int max, min;
        public int maximumDifference;

        public Difference(int[] elements) {
            this.elements = elements;
            max = min = 0;
        }

        public int computeDifference(){
            Arrays.sort(elements);
            max = elements[elements.length-1];
            min = elements[0];
            maximumDifference = max - min;
            return maximumDifference;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        sc.close();

        Difference difference = new Difference(a);

        difference.computeDifference();

        System.out.print(difference.maximumDifference);
    }
}
