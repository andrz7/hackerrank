package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/13/2016.
 */
public class Day12 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String firstName = scan.next();
        String lastName = scan.next();
        int id = scan.nextInt();
        int numScores = scan.nextInt();
        int[] testScores = new int[numScores];
        for(int i = 0; i < numScores; i++){
            testScores[i] = scan.nextInt();
        }
        scan.close();

        Student s = new Student(firstName, lastName, id, testScores);
        s.printPerson();
        System.out.println("Grade: " + s.calculate());
    }
}

class Person {
    protected String firstName;
    protected String lastName;
    protected int idNumber;

    // Constructor
    Person(String firstName, String lastName, int identification){
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = identification;
    }

    // Print person data
    public void printPerson(){
        System.out.println(
                "Name: " + lastName + ", " + firstName
                        + 	"\nID: " + idNumber);
    }

}

class Student extends Person{
    private static int[] testScores;

    public Student(String firstName, String lastName, int identification, int[] numScores) {
        super(firstName, lastName, identification);
        this.testScores = numScores;
    }

    public static char calculate(){
        int averageScore = 0;
        int sumScore = 0;
        char grade;

        for (int i = 0 ; i < testScores.length ; i++){
            sumScore += testScores[i];
        }
        averageScore = sumScore / testScores.length;

        if (averageScore >= 90 && averageScore <= 100)
            grade = 'O';
        else if (averageScore >= 80 && averageScore < 90)
            grade = 'E';
        else if (averageScore >= 70 && averageScore < 80)
            grade = 'A';
        else if (averageScore >= 55 && averageScore < 70)
            grade = 'P';
        else if (averageScore >= 40 && averageScore < 55)
            grade = 'D';
        else
            grade = 'T';

        return grade;
    }
}
