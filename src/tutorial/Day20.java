package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/20/2016.
 */
public class Day20 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a[] = new int[n];
        int numberOfSwap = 0;
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }

        for(int i = 0 ; i < n ; i++){
            for(int j = 0 ; j < n-1 ; j++){
                if (a[j] > a[j+1]){
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                    numberOfSwap++;
                }
            }
            if (numberOfSwap == 0)
                break;
        }

        System.out.println("Array is sorted in " + numberOfSwap + " swaps.");
        System.out.println("First Element: " + a[0]);
        System.out.println("Last Element: " + a[n-1]);
    }

}
