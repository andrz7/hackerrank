package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/17/2016.
 */
public class Day17 {
    public static void main(String []argh)
    {
        Scanner in = new Scanner(System.in);
        int T=in.nextInt();
        while(T-->0)
        {
            int n = in.nextInt();
            int p = in.nextInt();
            Power myCalculator = new Power();
            try
            {
                int ans=myCalculator.power(n,p);
                System.out.println(ans);

            }catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }

    }
}
class Power {

    public Power() {
    }

    public int power(int n, int p) throws NumberFormatException{
        if (n < 0 || p < 0)
            throw new NumberFormatException("n and p should be non-negative");
        return (int) Math.pow(n, p);
    }
}
