package tutorial;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by SRIN on 6/29/2016.
 */
public class Day28 {

    static List<String> database;
    static private String REGEX = "@";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        database = new ArrayList<>();

        for(int a0 = 0; a0 < N; a0++){
            String firstName = in.next();
            String emailID = in.next();
            insertToList(firstName, emailID);
        }
        Collections.sort(database);
        for(String s : database){
            System.out.println(s);
        }
    }

    public static void insertToList(String firstName, String emailId){
        if (emailId.contains("@gmail.com")){
            database.add(firstName);
        }
    }
}
