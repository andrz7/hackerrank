package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/13/2016.
 */
public class Day10 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(String.valueOf(count(n)));
    }

    static int count(int n){
        int count = 0, max = 0;
        String binary = Integer.toBinaryString(n);
        for (int i = 0 ; i < binary.length() ;i++){
            if (binary.charAt(i) == '1'){
                count++;
            }else {
                count = 0;
            }
            if (max < count){
                max  = count;
            }
        }
        return max;
    }
}
