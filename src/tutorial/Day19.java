package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/20/2016.
 */
public class Day19 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        scan.close();
        AdvancedArithmetic myCalculator = new Calculator();
        int sum = myCalculator.divisorSum(n);
        System.out.println("I implemented: " + myCalculator.getClass().getInterfaces()[0].getName() );
        System.out.println(sum);
    }
}

interface AdvancedArithmetic{
    int divisorSum(int n);
}

class Calculator implements  AdvancedArithmetic{

    public Calculator() {
    }

    @Override
    public int divisorSum(int n) {
        int increment = 1;
        int sum = 0;

        if (n % 2 != 0){
            increment = 2;
        }

        for (int i = 1 ; i <= n ; i = i + increment){
            if (n % i == 0)
                sum += i;
        }

        return sum;
    }
}


