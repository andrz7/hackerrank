package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/9/2016.
 */
public class Day9 {

    private static int factorial(int N){
        if (N < 2)
            return 1;
        return N * factorial(N-1);
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        int result = factorial(N);
        System.out.println(result);
    }
}
