package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/29/2016.
 */
public class Day29 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            int k = in.nextInt();
            System.out.println(String.valueOf(maxBitwise(n, k)));
        }


    }

    public static int maxBitwise(int n , int k){
        int max = 0;
        int temp = 0;
        for (int i = 1 ; i <= n ; i++){
            for (int j = i+1 ; j <= n ;j++){
                temp = i & j;
                if (temp > max && temp < k) max = temp;
            }
        }
        return max;
    }
}
