package tutorial;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by SRIN on 6/13/2016.
 */
public class Day11 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arr[][] = new int[6][6];
        for(int i=0; i < 6; i++){
            for(int j=0; j < 6; j++){
                arr[i][j] = in.nextInt();
            }
        }
        System.out.println(String.valueOf(sum(arr, 6)));
    }

    public static int sum(int[][] arr, int n){
        int[] sum = new int[16];
        int count = 0, max = 0;
        for (int i = 0 ; i < 4 ; i++){
            for (int j = 0 ; j < 4 ; j++){
                sum[count]= arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2];
                count++;
            }
        }
        Arrays.sort(sum);
        return sum[15];
    }

}
