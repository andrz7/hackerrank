package tutorial;

import java.util.Scanner;

/**
 * Created by SRIN on 6/29/2016.
 */
public class Day27 {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
            Scanner in = new Scanner(System.in);
            int d1 = in.nextInt();
            int m1 = in.nextInt();
            int y1 = in.nextInt();
            int d2 = in.nextInt();
            int m2 = in.nextInt();
            int y2 = in.nextInt();
            int fine = 0;
            if (y1 - y2 > 0) fine = 10000;
            else if (y1-y2 == 0 && m1-m2 > 0) fine = 500 * (m1 - m2);
            else if (y1 - y2 == 0 && m1 - m2==0 && d1-d2 > 0) fine = 15 * (d1 - d2);
            System.out.println(fine);
    }
}
