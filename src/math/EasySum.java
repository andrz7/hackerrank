package math;

import java.util.Scanner;

public class EasySum {
	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
		Scanner scan = new Scanner(System.in);
		int T, N , m;
		long sum;
		T = scan.nextInt();
		for (int i = 0; i < T; i++) {
			sum = 0;
			N = scan.nextInt();
			for (int j = 1; j < N; j++) {
				sum += ( j % 5);
			}
			System.out.println(sum);
		}
	}
	

}
