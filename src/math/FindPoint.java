package math;

import java.util.Scanner;

public class FindPoint {
	 public static void main(String[] args) {
	     Scanner scan = new Scanner(System.in);
	     int pX, pY, qX, qY, T, x, y;
	     T = scan.nextInt();
	     for(int i = 0 ; i < T ;i++){
	    	 pX = scan.nextInt();
		     pY = scan.nextInt();
		     qX = scan.nextInt();
		     qY = scan.nextInt();
		     x = 2 * qX - pX;
		     y = 2 * qY - pY;
		     System.out.println(x + " " + y);
	     }
	    
	    }
}
