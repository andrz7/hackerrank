package math;

import java.util.Scanner;

public class Handshake {

	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
		Scanner scan = new Scanner(System.in);
		int t, p;
		t = scan.nextInt();
		for (int i = 0; i < t; i++) {
			p = scan.nextInt();
			if ( p == 0){
				System.out.println("0");
			}else if (p >= 1){
				int total = (p-1)*p/2;
				System.out.println(total);
			}
		}
	}
	
}
