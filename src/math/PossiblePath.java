package math;

import java.util.Scanner;

public class PossiblePath {
	public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
		Scanner scan = new Scanner(System.in);
		
		int T, x1, y1, x2, y2;
		
		T = scan.nextInt();
		
		for(int i = 0 ; i < T ; i++){
			x1 = scan.nextInt();
			y1 = scan.nextInt();
			x2 = scan.nextInt();
			y2 = scan.nextInt();
			
			if (GCD(x1, y1) == GCD(x2, y2)) {
				System.out.println("YES");
			}else
				System.out.println("NO");
		}
	}
	
	public static int GCD(int a, int b){
		if (b == 0) return a;
		return GCD(b, a%b);
	}
}
