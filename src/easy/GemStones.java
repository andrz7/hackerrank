package easy;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class GemStones {
	public static void main(String[] args) {
	    Scanner scan  = new Scanner(System.in);
	    int numString = scan.nextInt();
	    int count = 0;
	    int [] arrayChars = new int[26];
	    Set<Character> charSet = new HashSet<Character>();

	    for(int i = 0; i< numString; i++) {
	        String s = scan.next();
	        for (char c : s.toCharArray()) {
	          charSet.add(c);
	        }
	        for (char temp : charSet) {
	            int x = (int)temp-97;
	            arrayChars[x]++;
	            if(arrayChars[x] >= numString){
	                count++;
	            }
	        }
	        charSet.clear();      
	    }
	    System.out.println(count); 
	}
}
