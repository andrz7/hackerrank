package easy;

import java.util.Scanner;


public class ShrelockAndArray {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();
		for (int k = 0 ; k < T ; k++){
			int N = scan.nextInt();
			int[] arr = new int[N];	
			for (int i = 0; i < arr.length; i++) {
				arr[i] = scan.nextInt();
			}
			int sum = 0;
			for (int i = 0 ; i < arr.length ;i++)
				sum += arr[i];
			int leftSum = 0;
			int rightSum = sum;
			int flag = 0;
			for (int i = 0 ; i < arr.length; i++){
				leftSum = sum - rightSum;
				rightSum = rightSum - arr[i];
				if (leftSum == rightSum){
					flag = 1;
					break;				
				}
			}
			if (flag == 1)
				System.out.println("YES");
			else 
				System.out.println("NO");
		}
	}
	
}
