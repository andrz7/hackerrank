package easy;

import java.util.Scanner;


public class UtopianTree {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int height;
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            height = 1;
            for(int i = 1 ; i <= n ; i++ ){
            	if (i % 2 == 0) height += 1;
            	else height *= 2;
            }
            System.out.println(height);
        }
    }
}
