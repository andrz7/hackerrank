package easy;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class Pangram {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String s = scan.nextLine();
		s = s.replaceAll("\\s", "");
		s = s.toUpperCase();
		Set<Character> hashSet = new HashSet<Character>();
		for (int i = 0 ; i < s.length() ; i++)
			hashSet.add(s.charAt(i));
		if (hashSet.size() >= 26)
			System.out.println("pangram");
		else
			System.out.println("not pangram");
	}
}
