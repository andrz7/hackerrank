package easy;

import java.util.Scanner;


public class CaesarChipper {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String s = in.next();
        int k = in.nextInt();
        for (char indexChar : s.toCharArray()) {
            if (Character.isLetter(indexChar)) {
                char baseChar = (Character.isUpperCase(indexChar)) ?
                    'A' : 'a'; 
                System.out.print((char)
                    (baseChar + (indexChar - baseChar + k)
                     % 26));
                }
            else
                System.out.print(indexChar);
        }
    }
}
