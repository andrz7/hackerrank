package easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;


public class AcmIcpc {
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int max = 0;
        int n = in.nextInt();
        int m = in.nextInt();
        String topic[] = new String[n];
        for(int topic_i=0; topic_i < n; topic_i++){
            topic[topic_i] = in.next();
        }
        int count = 0;
        for (int i = 0 ; i < n-1 ;i++){
        	for (int j = i+1 ; j < n ;j++){
        		int topics = 0;
        		for (int k = 0 ; k < m ; k++){
        			if (topic[i].charAt(k) == '1' || topic[j].charAt(k) == '1')
        				topics += 1;
        		}
        		if (topics > max){
        			max = topics;
        			count = 1;
        		}else if (topics == max)
        			count++;
        	}
        }
        System.out.println(max);
        System.out.println(count);
    }


}
