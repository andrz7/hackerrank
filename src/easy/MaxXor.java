package easy;

import java.util.Scanner;


public class MaxXor {
    static int maxXor(int l, int r) {
        int xor = l ^ r;
        System.out.println(xor);
        int n = 0;
        while (Math.pow(2, n) <= xor) n++;
        return (int)Math.pow(2, n)-1;

    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int res;
        int _l;
        _l = Integer.parseInt(in.nextLine());
        
        int _r;
        _r = Integer.parseInt(in.nextLine());
        
        res = maxXor(_l, _r);
        System.out.println(res);
        
    }
}
