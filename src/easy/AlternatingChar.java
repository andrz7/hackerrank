package easy;

import java.util.Scanner;


public class AlternatingChar {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();
		String s;
		for (int j = 0 ; j < T ; j++){
			s = scan.next();
			int count = 0;
			for (int i = 0 ; i < s.length()-1 ; i++){
				if (s.charAt(i) == s.charAt(i+1))
					count++;
			}
			System.out.println(count);
		}
	}
}
