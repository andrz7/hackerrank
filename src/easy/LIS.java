package easy;

import java.util.Scanner;


public class LIS {
	int[] lis = new int[100];
	int[] arr = new int[100];
	Scanner scan = new Scanner(System.in);

	public LIS(){
		int n = scan.nextInt();
		int temp = 0;
		for (int i = 0; i < n ; i++) {
			arr[i] = scan.nextInt();
			lis[i] = 1;
		}
		for (int i = 2 ; i <= n; i++){
			for (int j = 1; j <= i ;j++){
				if (arr[i] > arr[j])
					lis[i] += 1;
			}
			if (lis[i] > temp)
				temp = lis[i];
		}
		System.out.println(temp);
		
	}
	
	public static void main(String[] args) {
		new LIS();
	}
}
