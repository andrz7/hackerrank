package easy;

import java.util.Scanner;


public class WineSeller {
	Scanner scan = new Scanner(System.in);
	int cache[][];
	int v[];
	int profit(int year, int start, int stop){
		if(start > stop) return 0;
		if (cache[start][stop] != -1) return cache[start][stop];
		else
			return (Math.max(profit(year + 1, start + 1, stop) + year * 
					v[start], profit(year + 1, start, stop -1) + year * v[stop]));
	}
	
	public WineSeller(){
		int year, start, stop;
		int n = scan.nextInt();
		ksongin();
		year = scan.nextInt();
		start = scan.nextInt();
		stop = scan.nextInt();
		for (int i = 0 ; i < n ; i++)
			v[i] = scan.nextInt();
		profit(year, start, stop);
	}
	
	public void ksongin(){
		for (int i = 0; i < cache.length; i++) {
			for (int j = 0; j < cache.length; j++) {
				cache[i][j] = -1;
			}
		}
	}
	
	public static void main(String[] args) {
		new WineSeller();
	}
}
