package easy;

import java.lang.reflect.Array;
import java.util.Scanner;


public class LoveLetterMystery {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int counter = 0;
		int T = scan.nextInt();
		char[] words, reverseWords;
		for (int i = 0; i < T ; i++) {
			counter = 0;
			String s = scan.next();
			String reverS = new StringBuffer(s).reverse().toString();
			words = s.toCharArray();
			reverseWords = reverS.toCharArray();
			for (int j = 0 ; j < (words.length/2); j++)
				counter += Math.abs(words[j] - reverseWords[j]);
			System.out.println(counter);
		}
	}
}
