package easy;

import java.util.Scanner;


public class CavityMap {
	public static void main(String[] args) {
	    Scanner in = new Scanner(System.in);
	    int n = in.nextInt();
	    String grid[] = new String[n];
	    for(int grid_i=0; grid_i < n; grid_i++){
	        grid[grid_i] = in.next();
	    }
	    for(int i = 1; i < n-1; i++){
	        for(int j = 1; j < n-1; j++){
	            char center = grid[i].charAt(j);
	            char up = grid[i-1].charAt(j);
	            char down = grid[i+1].charAt(j);
	            char left = grid[i].charAt(j-1);
	            char right = grid[i].charAt(j+1);
	            if(center > up && center > down && center > left && center > right && up != 'X' && down != 'X' && right != 'X' && left != 'X') 
	                grid[i] = grid[i].substring(0,j) + "X" + grid[i].substring(j+1);
	        }
	    }
	    for(String s : grid) System.out.println(s);
	}
	
}
