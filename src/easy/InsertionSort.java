package easy;

import java.util.Scanner;


public class InsertionSort {
	static int[] ar;
    public static void insertionSortPart2(int n)
    {       
    	int i, j;
    	for (i = 1 ; i < n ;i++){
    		j = i;
    		while ( (j > 0) && (ar[j] < ar[j-1]) )
    		{
    			swap(ar, j, j-1);
    			j = j - 1;
    		}
    		printArray();
    	}
    }  
    
    public static void main(String[] args) {
       Scanner in = new Scanner(System.in);
       int s = in.nextInt();
       ar = new int[s];
       for(int i=0;i<s;i++){
            ar[i]=in.nextInt(); 
       }
       insertionSortPart2(s);    
                    
    }    
    private static void printArray() {
      for(int n: ar){
         System.out.print(n+" ");
      }
        System.out.println("");
   }
    
    public static void swap(int[] arr, int pointedNumber, int beforeNumber){
    	int temp;
    	temp = ar[pointedNumber];
    	ar[pointedNumber] = ar[beforeNumber];
    	ar[beforeNumber] = temp;
    }
}
