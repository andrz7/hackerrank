package easy;

import java.util.Scanner;


public class SherlockandSquares {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
        int testCases, A = 0, B = 0, count = 0;
        int temp;
        testCases = scan.nextInt();
        for (int i = 0; i < testCases; i++){
			A = scan.nextInt();
			B = scan.nextInt();
	        temp = (int) (Math.floor(Math.sqrt(B)) - Math.ceil(Math.sqrt(A)) + 1);
	        System.out.println(temp);
        }
    }
}
