package easy;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;


public class Kaprekar {
	public static void main(String[] args) {
		int square, temp, counter = 0, rem, quo;
		ArrayList<Integer> kaprekarStorage;
		
		Scanner scan = new Scanner(System.in);
        IsKaprekar isKaprekar = new IsKaprekar();
        
		int minValue = scan.nextInt();
		int maxValue = scan.nextInt();
		
		while (minValue <= maxValue){
			isKaprekar.countKaprekar(minValue);
			minValue++;
//			System.out.println(minValue);
		}
		
		kaprekarStorage = isKaprekar.getKaprekar();
		if (kaprekarStorage.size() == 0) {
			System.out.println("INVALID RANGE");
		}else{
			for (int i : kaprekarStorage){
				System.out.print(i + " ");
			}
		}
		
    }
	
	public static class IsKaprekar{
		
		ArrayList<Integer> arrNumber;
		
		public IsKaprekar() {
			arrNumber = new ArrayList<Integer>();
		}
		
		public void countKaprekar(int number){
			
			int counter = 0, temp;
			long square, leftPart, rightPart;
			
			temp = number;
			square = (long) number * number;
			
			while (number != 0){
				counter++;
				number = number/10;
			}
			
			rightPart = square % ((long) Math.pow(10, counter));
			leftPart = square / ((long) Math.pow(10, counter));
			
			if ((rightPart + leftPart) == temp)
				arrNumber.add(temp);
		}
		
		public ArrayList<Integer> getKaprekar(){
			return arrNumber;
		}
	}
}
