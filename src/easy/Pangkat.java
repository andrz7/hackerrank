package easy;

public class Pangkat {
	
	public int pangkat(int angka, int n){
		if (n == 1)
			return angka;
		return angka*pangkat(angka, n-1);
	}
	
	public int factorial (int n){
		if (n == 1)
			return 1;
		return n*factorial(n-1);
	}
	
	public int fibonaci(int n){
		//DFS karna dikerjain satu dulu ampe mentok abaru ke atas
		if (n <= 2)
			return 1;
		return (fibonaci(n-1) + fibonaci(n-2));
	}
	
	public Pangkat() {
		int i = 10, n = 6;
		System.out.println(pangkat(i, n));
		System.out.println(factorial(n));
		System.out.println(fibonaci(n));
	}
	
	public static void main(String[] args) {
		new Pangkat();
	}
}
