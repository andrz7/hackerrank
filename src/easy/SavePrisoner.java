package easy;

import java.util.Scanner;

/**
 * Created by SRIN on 7/12/2016.
 */
public class SavePrisoner {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int T = scan.nextInt();

        while(T-- > 0){
            long N = scan.nextLong();
            long M = scan.nextLong();
            long S = scan.nextLong();


            System.out.println(String.format("%d", (M + S - 2) % N + 1));
        }
    }
}
