package easy;

import java.util.Scanner;

/**
 * Created by SRIN on 6/7/2016.
 */
public class IcecreamParlor {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int T = scan.nextInt();
        int[] flavours;
        for (int i = 0 ; i < T ; i++){
            boolean isFounded = false;
            int M = scan.nextInt();
            int N = scan.nextInt();
            flavours = new int[N+1];
            for (int j = 1 ; j <= N ; j++){
                flavours[j] = scan.nextInt();
            }
            for (int k = 1 ; k <= N ; k++){
                for (int l = k+1 ; l <= N ; l++){
                    if (isFounded == true)
                        break;
                    if (flavours[k] + flavours[l] == M){
                        System.out.println((k) + " " + (l));
                        isFounded = true;
                    }
                }
            }
        }
    }

}
