package easy;

import java.util.Scanner;


public class Encryption {
	 public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        String s = in.next();
	        int columns, rows, wordsLength, index;
	        wordsLength = s.length();
	        rows = (int) Math.floor(Math.sqrt(wordsLength));
	        columns = (int) Math.ceil(Math.sqrt(wordsLength));
	        
	        if (rows * columns < wordsLength) {
				rows = columns;
			}
	        
	        for (int i = 0; i < columns; i++) {
	        	for (int j = 0; j < rows; j++) {
					index = i + j * columns;
					if (index < wordsLength) {
						System.out.print(s.charAt(index));	
					}
				}
	        	System.out.print(" ");
			}
	        
	    }
}
