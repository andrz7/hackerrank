package easy;

import java.util.Scanner;


public class FunnyString {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    int flag;
    Scanner scan = new Scanner(System.in);
    int test = scan.nextInt();
	    for (int i = 0 ; i < test ; i++){
	    	flag = 0;
	    	String s = scan.next();
	    	String r = new StringBuilder(s).reverse().toString();
		    for (int j = 1; j <= s.length() - 1; j++) {
		    	int value1 = Math.abs((int)s.charAt(j) - (int)s.charAt(j-1));
		    	int value2 = Math.abs((int)r.charAt(j) - (int)r.charAt(j-1));
				if (value1 == value2)
					flag = 1;
				else{
					flag = -1;
					break;
				}	
			}
		    if (flag == 1) System.out.println("Funny");
		    else if (flag == -1) System.out.println("Not Funny");
	    }
    }
}
