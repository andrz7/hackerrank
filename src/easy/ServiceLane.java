package easy;

import java.util.Scanner;


public class ServiceLane {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int t = in.nextInt();
        int width[] = new int[n];
        for(int width_i=0; width_i < n; width_i++){
            width[width_i] = in.nextInt();
        }
        for(int a0 = 0; a0 < t; a0++){
            int i = in.nextInt();
            int j = in.nextInt();
            int temp = Math.min(width[i], width[i+1]);
            for (int k = i+1; k < j; k++) {
            	if (temp  > width [k+1])
            		temp = width[k+1];
			}
            if (temp == 1)
            	System.out.println("1");
            else if (temp == 2)
            	System.out.println("2");
            else if (temp == 3)
            	System.out.println("3");
        }
    }

}
