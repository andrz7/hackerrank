package easy;

import java.util.Scanner;


public class FindDigits {
    public static void main(String[] args) {
    	String  digits;
    	int counter = 0;
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            digits = Integer.toString(n);
            counter = 0;
            for(int i = 0 ; i <= digits.length()-1 ;i++) {
            	int temp = Character.getNumericValue(digits.charAt(i));
            	if(temp != 0){
	            	if(n % temp == 0)
	            		counter++;
            	}
            }
           System.out.println(counter);
        }
    }
}
