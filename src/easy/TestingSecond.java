package easy;

import java.util.Scanner;


public class TestingSecond {
    static String hour;
	static String minute;
	static String second;
    static String[] times;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String time = in.next();
        times = time.split(":");
        hour = String.valueOf(Integer.parseInt(times[0]) + 12);
        minute = times[1];
        second = times[2];
        second = second.substring(0, 2);
        System.out.println(hour+":"+minute+":"+second);
    }
}
