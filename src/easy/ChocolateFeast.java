package easy;

import java.util.Scanner;


public class ChocolateFeast {
	
	 public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int temp, chocolateGet, wrappers;
	        int t = in.nextInt();
	        for(int a0 = 0; a0 < t; a0++){
	            int n = in.nextInt();
	            int c = in.nextInt();
	            int m = in.nextInt();
	            chocolateGet = 0;
	            temp = n / c;
	            wrappers = temp;
	            while(wrappers >= m){
	            	temp += (wrappers/m);
	            	int left = wrappers % m;
	            	wrappers = (wrappers/m) + left;
	            }
	        System.out.println(temp);
	    }
	 }
}
