package easy;

import java.util.Scanner;


public class TestingOne {
	public static void main(String[] args) {
        float plus = 0,minus = 0,zero = 0;
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int arr[] = new int[n];
        for(int arr_i=0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
            if(arr[arr_i] < 0)
                minus++;
            else if (arr[arr_i] == 0)
                zero++;
            else 
                plus++;
        }
        System.out.println((float)plus/n);
        System.out.println((float)minus/n);
        System.out.println((float)zero/n);
    }
}
