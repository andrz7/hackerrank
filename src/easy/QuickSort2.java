package easy;

import java.util.ArrayList;
import java.util.Scanner;


public class QuickSort2 {
	
		static int partition(int[] ar, int minPos, int maxPos){
			int p = ar[minPos];
			ArrayList<Integer> leftList = new ArrayList<Integer>();
			ArrayList<Integer> rightList = new ArrayList<Integer>();
					
			for (int i = minPos + 1 ; i <= maxPos ; i ++){
				if (ar[i] > p)
					rightList.add(ar[i]);
				else
					leftList.add(ar[i]);
			}
			
			copy(leftList, ar, minPos);
			int pPos = leftList.size() + minPos;
			ar[pPos] = p;
			copy(rightList, ar, pPos+1);
			
			return minPos + leftList.size();
		}
		
		static void copy(ArrayList<Integer> list, int[] ar, int index){
			for (int i : list){
				ar[index++] = i; 
			}
		}
		
		 static void quickSort(int[] ar, int low ,int high) {
		     if (low >= high)
		    	 return;
		     int position = partition(ar, low, high);
		     
		     quickSort(ar, low, position-1);
		     quickSort(ar, position+1, high);
		     
		     printArray(ar, low, high);
		 } 
		 
		 static void quickSort(int[] ar){
			 quickSort(ar, 0, ar.length - 1);
		 }
		
		static void printArray(int[] ar, int start, int end) {
		   for(int i = start ; i <= end ;i++){
		      System.out.print(ar[i]+" ");
		   }
		     System.out.println("");
		}
		 
		public static void main(String[] args) {
		     Scanner in = new Scanner(System.in);
		     int n = in.nextInt();
		     int[] ar = new int[n];
		     for(int i=0;i<n;i++){
		        ar[i]=in.nextInt(); 
		     }
		     quickSort(ar);
		     
		 }    

}
