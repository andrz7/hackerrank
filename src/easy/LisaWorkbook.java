package easy;

import java.util.Scanner;

public class LisaWorkbook {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int numberOfChapter = scan.nextInt();
		int maxNumberOfProblem = scan.nextInt();
		int[] arr = new int[numberOfChapter];
		int page = 0, count = 0, numberOfProblem;
		for (int i = 1; i <= numberOfChapter; i++) 
			arr[i-1] = scan.nextInt();
		for (int k = 1 ; k <= numberOfChapter ; k++){
			page++;
			numberOfProblem = arr[k-1];
			for (int j = 1 ; j <= numberOfProblem ; j++){
				if (j == page) 
					count++;
				if (j % maxNumberOfProblem == 0 && j < numberOfProblem) 
					page++;
			}
		}
		System.out.println(count);
	}
}
