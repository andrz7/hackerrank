package easy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class SortingIntro {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int foundInt = scan.nextInt();
		int arrLength = scan.nextInt();
		List<Integer> arrList = new ArrayList<Integer>(arrLength);
		for (int i = 0 ; i < arrLength ; i++)
			arrList.add(scan.nextInt());
		System.out.println(arrList.indexOf(foundInt));
	}
}
