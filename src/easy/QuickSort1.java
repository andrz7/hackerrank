package easy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class QuickSort1 {

    static void partition(int[] ar, int n) {
	      int pivot = 0;
	      ArrayList<Integer> left = new ArrayList<Integer>();
	      ArrayList<Integer> right = new ArrayList<Integer>();
	      for (int i = 0 ; i < ar.length ; i++){
	    	  pivot = ar[0];
	    	  if (ar[i] >= pivot)
	    		  right.add(ar[i]);
	    	  else 
	    		  left.add(ar[i]);
	      }
	      printArray(left);
	      printArray(right);
	 }   
	
	static void printArray(ArrayList<Integer> ar) {
	   for(int n: ar){
	      System.out.print(n+" ");
	   }
	}
	 
	public static void main(String[] args) {
	     Scanner in = new Scanner(System.in);
	     int n = in.nextInt();
	     int[] ar = new int[n];
	     for(int i=0;i<n;i++){
	        ar[i]=in.nextInt(); 
	     }
	     partition(ar, n);
	 }    
}
