package Medium;

import java.math.BigInteger;
import java.util.Scanner;

public class ExtraLongFactorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(factorial(n));
    }
    
    public static BigInteger factorial (int n)
    {
    	BigInteger bi = new BigInteger("1");
    	for (int i = 1; i <= n; i++) 
    	{
			bi = bi.multiply(new BigInteger(i+""));
		}
    	return bi;
    }
}
