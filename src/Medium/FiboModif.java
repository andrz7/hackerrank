package Medium;

import java.math.BigInteger;
import java.util.Scanner;

public class FiboModif {
	
	 public static void main(String[] args) {
		 	Scanner scan = new Scanner(System.in);
		 	int A = scan.nextInt();
		 	int B = scan.nextInt();
		 	int N = scan.nextInt();
		 	BigInteger res = fibonaci (A, B, --N);
	        System.out.println(res);
	    }
	 
	 static BigInteger fibonaci (int A, int B, int N){
		 BigInteger res;
		 if (N == 0)
			 res = new BigInteger(String.valueOf(A));
		 else if (N == 1)
			 res = new BigInteger(String.valueOf(B));
		 else
			 res = fibonaci(A, B, N-1).pow(2).add(fibonaci(A, B, N-2));
		 return res;
	 }
}
