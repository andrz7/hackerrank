package Medium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class AngryChicldren {
	public static void main(String[] args) throws NumberFormatException, IOException {

	      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	      int N = Integer.parseInt(in.readLine());
	      int K = Integer.parseInt(in.readLine());
	      int[] list = new int[N];
	      
	      for(int i = 0; i < N; i ++)
	         list[i] = Integer.parseInt(in.readLine());
	      
	      int unfairness = Integer.MAX_VALUE;
	      Arrays.sort(list);
	      int minValue = list[0], maxValue = 0;
	      for (int i = 0; i < N-K+1; i++) {
			minValue = list[i];
			maxValue = list[i+K-1];
			if ((maxValue - minValue) < unfairness)
				unfairness = maxValue - minValue;
	      }
	      System.out.println(unfairness);
	   }
}
