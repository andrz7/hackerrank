package Medium;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class LarryArray {
	
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int T = scan.nextInt();			
		final int N = 1509;
		int n;
		int[] a = new int[N];
		while (T > 0){
			n = scan.nextInt();
			for (int i = 1; i <= n; i++) 
				a[i] = scan.nextInt();
			
			boolean K=true;
		    for (int i=1;i<=n;i++)
		    for (int j=i+1;j<=n;j++)
		        K^=(a[i]>a[j]);
		    
		    if (K) System.out.println("YES");
		    else System.out.println("NO");
		    T--;
		}
		
		
	}
}
