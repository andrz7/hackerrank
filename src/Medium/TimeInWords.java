package Medium;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Scanner;

public class TimeInWords {
	public static void main(String[] args) {
	    Scanner in = new Scanner(System.in);
	    int h = in.nextInt();
	    int m = in.nextInt();
	    assert h>=1 && h<12;
	    assert m>=0 && h<60;

	    HashMap<Integer,String> hm=new HashMap<Integer,String>();
	    hm.put(0,"o' clock");
	    hm.put(1,"one");
	    hm.put(2,"two");
	    hm.put(3,"three");
	    hm.put(4,"four");
	    hm.put(5,"five");
	    hm.put(6,"six");
	    hm.put(7,"seven");
	    hm.put(8,"eigth");
	    hm.put(9,"nine");
	    hm.put(10,"ten");
	    hm.put(11,"eleven");
	    hm.put(12,"twelve");
	    hm.put(13,"thirteen");
	    hm.put(14,"fourteen");
	    hm.put(15,"quarter");
	    hm.put(16,"sixteen");
	    hm.put(17,"seventeen");
	    hm.put(14,"eighteen");
	    hm.put(19,"nineteen");
	    hm.put(20,"twenty");
	    hm.put(20,"twenty");
	    hm.put(21,"twenty one");
	    hm.put(22,"twenty two");
	    hm.put(23,"twenty three");
	    hm.put(24,"twenty four");
	    hm.put(25,"twenty five");
	    hm.put(26,"twenty six");
	    hm.put(27,"twenty seven");
	    hm.put(28,"twenty eight");
	    hm.put(29,"twenty nine");
	    hm.put(30,"half");

	    if(m==0) System.out.println(hm.get(h)+" "+hm.get(m));
	    else if(m==15){
	        if(h==0){
	            if(m==1) System.out.println(hm.get(m)+" past "+"twelve");
	            else System.out.println(hm.get(m)+" past "+"twelve");
	        }
	        else{
	            if(m==1) System.out.println(hm.get(m)+" past "+hm.get(h));
	            else System.out.println(hm.get(m)+" past "+hm.get(h));
	        }
	    }
	    else if(m>=1 && m<30){
	        if(h==0){
	            if(m==1) System.out.println(hm.get(m)+" minute "+"past "+"twelve");
	            else System.out.println(hm.get(m)+" minutes "+"past "+"twelve");
	        }
	        else{
	            if(m==1) System.out.println(hm.get(m)+" minute "+"past "+hm.get(h));
	            else System.out.println(hm.get(m)+" minutes "+"past "+hm.get(h));
	        }
	    }
	    else if(m==30){
	        if(h==0){
	            System.out.println(hm.get(m)+" past "+"twelve");
	        }
	        else{
	            System.out.println(hm.get(m)+" past "+hm.get(h));
	        }
	    }
	    else if(m==45){
	        m=60-m;
	        h=h+1;
	        if(m==1) System.out.println(hm.get(m)+" to "+hm.get(h));
	        else System.out.println(hm.get(m)+" to "+hm.get(h));            
	    }            
	    else{
	        m=60-m;
	        h=h+1;
	        if(m==1) System.out.println(hm.get(m)+" minute "+"to "+hm.get(h));
	        else System.out.println(hm.get(m)+" minutes "+"to "+hm.get(h));
	    }        
	}
}
